// Bubble sort allows to sort through different values by comparing two elements at a time and then swaps the elements in the array until all elements are sorted in a specified manner

// Define the array to sort
let array = [9, 6, 8, 2, 1, 7, 4, 3]
// Define the current size of the array
let size = array.length

// Console log the original array at the start of the sort
console.log(array)

// Create a for loop that will print the current iteration count
for (let i = 0; i < size - 1; i++) {
    // Console log the current iteration count of the loop
    console.log('Iteration Count: ' + (i + 1))

    // Create a for loop that will compare two elements of the array in succession and swap the position of the numbers if the current value being checked is larger
    for (let j = 0; j < size - 1; j++) {

        //Define the current element to be compared
        let currentElement = array[j]
        //Define the next element in the array to be compared
        let nextElement = array[j + 1]

        // Console log the two elements being compared
        console.log('Comparing ' + currentElement + ' and ' + nextElement)
        
        // Create a conditional statement that will swap the numbers if the current element is larger than the next element
        if (currentElement > nextElement) {

            // Console log a message that tells us which number is larger
            console.log(currentElement + ' is larger than ' + nextElement + '. Swapping elements.')

            // Swaps the two elements in the array
            array[j] = nextElement
            array[j + 1] = currentElement

            // Console log the current order of the array
            console.log(array)
        } else {
            // Console log a message that tells us that the numbers aren't swapped if the current element is smaller
            console.log(currentElement + ' is lesser than ' + nextElement + '. No swapping done.')
        }
    }
}

// Console log the final output of the array
console.log(array)