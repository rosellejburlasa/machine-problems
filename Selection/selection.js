// Selection sort allows to sort through different values and "selects" the a number and then "sorts" the items/elements in a specified manner
//This is done by taking the number and then storing it into a different array

// Define the array to sort
let originalArray = [9, 6, 8, 2, 1, 7, 4, 3]
// Define an empty array to store the sorted data in ascending order
let sortedArray = []
// Define a count variable to display the current iteration number
let count = 1

// Console log the original array at the start of the sort
console.log(originalArray)

// Create a while loop that will run while the originalArray still has an element
while (originalArray.length > 0) {
    //Console log the current iteration count
    console.log('Iteration Count: ' + count)

    //Assign the first item of the originalArray as the default smallest number
    let smallest = originalArray[0]

    // Create a for loop that will iterate through each element in the array to choose the smallest number and then save it in the "sortedArray"
    for (let i = 0; i < originalArray.length; i++) {
        //Create a condition that checks if the current smallest number is greater than the next element in the array
        //To sort the elements in ascending order use "<" instead
        if (smallest > originalArray[i]) {
            //Store the next element as the smallest number
            smallest = originalArray[i]
        }
    }

    //Console log the current smallest number found in the array
    console.log('Smallest number is: ' + smallest)

    // Add the smallest number for the original array to the new array
    sortedArray.push(smallest)
    // Remove the smallest number from the original array
    originalArray.splice(originalArray.indexOf(smallest), 1)
    
    // Add 1 to the counter for the iteration
    count++

}

// Console log the final output
console.log(sortedArray)